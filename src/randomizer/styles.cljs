(ns randomizer.styles)

(def styles {:main {:flex 1
                    :flex-direction "column"
                    :justify-content "center"
                    :align-items "center"
                    :background-color "#003049"}

             :rnd-button {:border-radius 300}

             :rnd-button-text {:font-size 300
                               :font-weight "bold"
                               :color "#EAE2B7"
                               :background-color "#003049"}

             :keep-awake-btn {:size 30
                              :color  "#EAE2B7"
                              :background-color "#003049"}})
