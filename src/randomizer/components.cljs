(ns randomizer.components
  (:require [reagent.core :as r]))

(def Entypo (js/require "@expo/vector-icons/Entypo"))
(def icon (r/adapt-react-class (.-default Entypo)))

(def expo (js/require "expo"))
(def keep-awake-comp (r/adapt-react-class (.. expo -Components -KeepAwake)))
