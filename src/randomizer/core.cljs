(ns randomizer.core
  (:require [reagent.core :refer [atom] :as r]
            [re-frame.core :refer [dispatch subscribe dispatch-sync]]
            [cljs-exponent.reagent :refer [text view touchable-highlight] :as rn]
            [cljs-exponent.font :as f]
            [randomizer.styles :refer [styles]]
            [randomizer.events]
            [randomizer.subs]
            [randomizer.components :refer [icon keep-awake-comp]]))

(defn app-root []
  (let [number (subscribe [:number])
        keep-awake? (subscribe [:keep-awake?])]
    (fn []
      [view {:style (styles :main)}

       (when @keep-awake?
         [keep-awake-comp])

       [touchable-highlight
        {:on-press #(dispatch [:rand-number])
         :style (styles :rnd-button)}
        [text {:style (styles :rnd-button-text)}
         (str @number)]]

       [touchable-highlight
        {:on-press #(dispatch [:toggle-keep-awake])}
        [icon (merge {:name (if @keep-awake? "eye-with-line" "eye")}
                     (styles :keep-awake-btn))]]])))

(defn init []
  (dispatch-sync [:initialize-db])
  (.registerComponent rn/app-registry "main" #(r/reactify-component app-root)))
