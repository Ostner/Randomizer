(ns randomizer.events
  (:require [re-frame.core :refer [reg-event-db]]
            [randomizer.db :refer [default-value]]))

(reg-event-db
 :initialize-db
 (fn [db _]
   default-value))

(reg-event-db
 :toggle-keep-awake
 (fn [db _]
   (update db :keep-awake? not)))

(reg-event-db
 :rand-number
 (fn [db _]
   (assoc db :number (rand-int 11))))
