(ns randomizer.subs
  (:require [re-frame.core :refer [reg-sub]]))

(reg-sub
 :keep-awake?
 (fn [db _]
   (:keep-awake? db)))

(reg-sub
 :number
 (fn [db _]
   (:number db)))
